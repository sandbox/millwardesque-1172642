<?php
// $Id$

// Defines for the variable / field names used by this plugin
define('IMAGESEARCHFIELD_GOOGLEIMAGESEARCH_KEY', 'imagesearchfield_googleimagesearch_api_key'); // Variable stores the Google Image Search API key
define('IMAGESEARCHFIELD_GOOGLEIMAGESEARCH_NUMRESULTS', 'imagesearchfield_googleimagesearch_num_results'); // Variable stores the num number of results to return from a Google Image Search

define('IMAGESEARCHFIELD_GOOGLEIMAGESEARCH_MAXRESULTS', 8); // Hard limit set by google on the maximum number of images allowed per page.  Since we only process one page, this is the same as the maximum number of images we can return 

/**
 * @file
 * Implements the imagesearch_engine plugins for integrating the Google Image Search API with the image-search field
 */

$plugin = array(
  'name' => 'googleimagesearch',
  'title' => 'Google Image Search API',
  'module' => 'imagesearchfield',
  'image query' => 'googleimagesearch_image_query',
  'settings form' => 'googleimagesearch_settings',
  'file' => 'googleimagesearch.inc',
);

/**
 * Form for the googleimagesearch settings
 * @param $form
 * @param $form_state
 * $param array $conf The current configuration of the plugin
 */
function googleimagesearch_settings(&$form, &$form_state, $conf = NULL) {
  drupal_set_title(t('Google Image Search plugin settings'));
  
  $form[IMAGESEARCHFIELD_GOOGLEIMAGESEARCH_KEY] = array(
    '#title' => t('Google API Key'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get(IMAGESEARCHFIELD_GOOGLEIMAGESEARCH_KEY, ''),
  );
  
  $form[IMAGESEARCHFIELD_GOOGLEIMAGESEARCH_NUMRESULTS] = array(
    '#title' => t('Number of results to return (Limit ' . IMAGESEARCHFIELD_GOOGLEIMAGESEARCH_MAXRESULTS . ')'),
    '#type' => 'textfield',
    '#size' => 4,
    '#required' => TRUE,
    '#default_value' => variable_get(IMAGESEARCHFIELD_GOOGLEIMAGESEARCH_NUMRESULTS, ''),
  );
  
  return system_settings_form($form);
}

/**
 * Validates the settings form for the Google Image Search plugin
 */
function googleimagesearch_settings_validate($form, &$form_state) {
  $num_results = $form_state['values'][IMAGESEARCHFIELD_GOOGLEIMAGESEARCH_NUMRESULTS]; 
  if (!(is_numeric($num_results) && (int)$num_results == $num_results) ||   // Ensure it's an integer value
        $num_results < 0 || $num_results > IMAGESEARCHFIELD_GOOGLEIMAGESEARCH_MAXRESULTS) {
    form_set_error(IMAGESEARCHFIELD_GOOGLEIMAGESEARCH_NUMRESULTS, t('The number of results must be an integer between 0 and ' . IMAGESEARCHFIELD_GOOGLEIMAGESEARCH_MAXRESULTS));
  }
}

/**
 * Performs a search for an image
 * @param array $query_terms An array of search terms
 * Returns an array of image URLs from the resulting search
 */
function googleimagesearch_image_query($query_terms) {
  $images = array();

  /**
   * For reference (in case site variable gets overwritten):
   * http://dev.albumart.millwardesque.com => ABQIAAAA6xk7AWEZe76iOVbJ2ME1LBTWUCcCFohmWGhfzyCi5H2Q3mdlSRSDfTzRxWPA5ok7jtQGnGOqX56ivA
   */
  $api_key = variable_get(IMAGESEARCHFIELD_GOOGLEIMAGESEARCH_KEY, '');
    
  // Build the query terms
  $query_string = '';
  foreach ($query_terms as $term) {
    /** 
     * This has the unfortunate side-effect of adding a trailing comma to the final string
     * but we'll remove it below
     */
    $query_string .= '"' . urlencode(trim($term)) . '",';
  }
  if ($query_string) {  // Strip the trailing comma
    $query_string = substr($query_string, 0, -1);
  }
  
  // Get the results
  $results = _googleimagesearch_performQuery($api_key, $query_string);
  
  // Ensure we got some images back
  if (!$results) {
    drupal_set_message(t('There was an unknown error fetching images from the Google Image Search API'), 'error');
    return array();
  }
  else if ($results->responseStatus != 200) {
    drupal_set_message(t('There was an error fetching images from the Google Image Search API: Error code @error_code.  Message: @message', array('@error_code' => $results->responseStatus, '@message' => $results->responseDetails)), 'error');
    return array();
  }
  
  // Create the array of images
  foreach ($results->responseData->results as $image) {
    $images[] = $image->unescapedUrl;
  }
  
  return $images;
}

/**
 * Performs a query using the Google Image Search API
 * Code adapted from official google documentation at
 * http://code.google.com/apis/imagesearch/v1/jsondevguide.html
 * @param string $api_key The website's API key
 * @param string $query_string The query string to search for 
 */
function _googleimagesearch_performQuery($api_key, $query_string) {
  $max_images_per_page = 8;  
  
  $user_ip = $_SERVER['REMOTE_ADDR'];
  $host = $_SERVER['HTTP_HOST'];
  $url = 'https://ajax.googleapis.com/ajax/services/search/images?' . 
        'v=1.0&key=' . $api_key . '&userip=' . $user_ip . '&q=' . $query_string . '&rsz=' . variable_get(IMAGESEARCHFIELD_GOOGLEIMAGESEARCH_NUMRESULTS, 1);
  
  // Make the request using curl 
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_REFERER, $host);
  $body = curl_exec($ch);
  curl_close($ch);
  
  // Decode the response and return
  $json = json_decode($body);
  return $json;
}
