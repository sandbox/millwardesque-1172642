<?php
// $Id$

// Defines for the variable / field names used by this plugin
define('IMAGESEARCHFIELD_BINGIMAGESEARCH_KEY', 'imagesearchfield_bingimagesearch_key'); // Variable stores the Bing Image Search API key
define('IMAGESEARCHFIELD_BINGIMAGESEARCH_NUMRESULTS', 'imagesearchfield_bingimagesearch_num_results'); // Variable stores the num number of results to return from a Bing Image Search

define('IMAGESEARCHFIELD_BINGIMAGESEARCH_MAXRESULTS', 50); // Hard limit set by bing on the maximum number of images allowed per page.  Since we only process one page, this is the same as the maximum number of images we can return 

/**
 * @file
 * Implements the imagesearch_engine plugins for integrating the Google Image Search API with the image-search field
 */

$plugin = array(
  'name' => 'bingimagesearch',
  'title' => 'Bing Image Search API',
  'module' => 'imagesearchfield',
  'image query' => 'bingimagesearch_image_query',
  'settings form' => 'bingimagesearch_settings',
  'file' => 'bingimagesearch.inc',
);

/**
 * Form for the bingimagesearch settings
 * @param $form
 * @param $form_state
 * $param array $conf The current configuration of the plugin
 */
function bingimagesearch_settings(&$form, &$form_state, $conf = NULL) {
  drupal_set_title(t('Bing Image Search plugin settings'));
  
  $form[IMAGESEARCHFIELD_BINGIMAGESEARCH_KEY] = array(
    '#title' => t('Bing API Key'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get(IMAGESEARCHFIELD_BINGIMAGESEARCH_KEY, ''),
  );
  
  $form[IMAGESEARCHFIELD_BINGIMAGESEARCH_NUMRESULTS] = array(
    '#title' => t('Number of results to return (Limit ' . IMAGESEARCHFIELD_BINGIMAGESEARCH_MAXRESULTS . ')'),
    '#type' => 'textfield',
    '#size' => 4,
    '#required' => TRUE,
    '#default_value' => variable_get(IMAGESEARCHFIELD_BINGIMAGESEARCH_NUMRESULTS, ''),
  );
  
  return system_settings_form($form);
}

/**
 * Validates the settings form for the Bing Image Search plugin
 */
function bingimagesearch_settings_validate($form, &$form_state) {
  $num_results = $form_state['values'][IMAGESEARCHFIELD_BINGIMAGESEARCH_NUMRESULTS]; 
  if (!(is_numeric($num_results) && (int)$num_results == $num_results) ||   // Ensure it's an integer value
        $num_results < 0 || $num_results > IMAGESEARCHFIELD_BINGIMAGESEARCH_MAXRESULTS) {
    form_set_error(IMAGESEARCHFIELD_BINGIMAGESEARCH_NUMRESULTS, t('The number of results must be an integer between 0 and ' . IMAGESEARCHFIELD_BINGIMAGESEARCH_MAXRESULTS));
  }
}

/**
 * Performs a search for an image
 * @param array $query_terms An array of search terms
 * Returns an array of image URLs from the resulting search
 */
function bingimagesearch_image_query($query_terms) {
  $images = array();

  /**
   * For reference (in case site variable gets overwritten):
   * AlbumArt key: DF51AB7F2CC45F0DDB10A2280E192726B8D38278
   */
  $api_key = variable_get(IMAGESEARCHFIELD_BINGIMAGESEARCH_KEY, '');
    
  // Build the query terms
  $query_string = '';
  foreach ($query_terms as $term) {
    /** 
     * This has the unfortunate side-effect of adding a trailing separator to the final string
     * but we'll remove it below
     */
    $query_string .= '"' . urlencode(trim($term)) . '"+';
  }
  if ($query_string) {  // Strip the trailing separator
    $query_string = substr($query_string, 0, -1);
  }
  
  // Get the results
  $results = _bingimagesearch_performQuery($api_key, $query_string);
  
  // Ensure we got some images back
  if (!$results || !isset($results->SearchResponse)) {
    drupal_set_message(t('There was an unknown error fetching images from the Bing Image Search API'), 'error');
    return array();
  }
  else if (isset($results->SearchResponse->Errors)) {
    // Show a message for each error
    foreach ($results->SearchResponse->Errors as $error) {
      // Create the message
      $message = '';
      foreach ($error as $error_detail) {
        $message .= $error_detail . ' : ';
      }
      drupal_set_message(t('There was an error fetching images from the Bing Image Search API: @message', array('@message' => $message)), 'error');
    }     
    return array();
  }
  
  // Create the array of images
  $num_results = 0; // Counter for number of images processed so far.  See comment in loop for more info
  foreach ($results->SearchResponse->Image->Results as $image) {
    $images[] = $image->MediaUrl;
    
    /**
     * For some reason, Bing sometimes returns more than the requested number of images.
     * This code should limit the results to the number we asked for
     */
    $num_results++;
    if ($num_results >= variable_get(IMAGESEARCHFIELD_BINGIMAGESEARCH_NUMRESULTS, 1)) {
      break;
    }
  }
  
  return $images;
}

/**
 * Performs a query using the Bing Image Search API
 * Code adapted from official documentation at
 * http://msdn.microsoft.com/en-us/library/dd250846.aspx
 * @param string $api_key The website's API key
 * @param string $query_string The query string to search for 
 */
function _bingimagesearch_performQuery($api_key, $query_string) {  
  $host = $_SERVER['HTTP_HOST'];
  $url = 'http://api.bing.net/json.aspx?' . 
        'AppID=' . $api_key . '&Version=2.2&Market=en-US&' . '&Query=' . $query_string . '&Sources=Image' . '&Image.CountSpecified=true&Image.Count=' . variable_get(IMAGESEARCHFIELD_BINGIMAGESEARCH_NUMRESULTS, 1);
  
  // Make the request using curl 
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_REFERER, $host);
  $body = curl_exec($ch);
  curl_close($ch);
  
  // Decode the response and return
  $json = json_decode($body);
  return $json;
}
