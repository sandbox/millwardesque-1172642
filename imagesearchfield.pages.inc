<?php
// $Id$

/**
 * @file
 * Page callbacks for the imagesearchfield module
 */

/**
 * Approves an image for a given field in a node
 * @param string $entity_type Type of entity
 * @param string field_name Name of the field
 * @param int $id ID of the entity
 * @param string $image_url Image URL of the approved field
 * Redirects to the entity's page when complete
 */
function imagesearchfield_approve_image($entity_type, $field_name, $id, $image_url) {
  $image_url = urldecode($image_url);
  
  // Load the entity
  $entities = entity_load($entity_type, array($id));
  
  // Update the fields
  $entity = $entities[$id];
  
  /**
   * @TODO: Fix to support more than one value per field
   */
  $field = &$entity->{$field_name}['und'][0];
  $field['url'] = $image_url;
  
  // Save the entity
  entity_save($entity_type, $entity);
  drupal_set_message(t("Image approved"));
  drupal_goto();
}

/**
 * Unsets any approved image for a given field in a node
 * @param string $entity_type Type of entity
 * @param string field_name Name of the field
 * @param int $id ID of the entity
 * Redirects to the entity's page when complete
 */
function imagesearchfield_unset_approved_image($entity_type, $field_name, $id) {
  // Load the entity
  $entities = entity_load($entity_type, array($id));
  
  // Update the fields
  $entity = $entities[$id];
  
  /**
   * @TODO: Fix to support more than one value per field
   */
  $field = &$entity->{$field_name}['und'][0];
  $field['url'] = NULL;
  
  // Save the entity
  entity_save($entity_type, $entity);
  drupal_set_message(t("Unset approved image"));
  drupal_goto();
}

/**
 * List of plugins available for image searches
 */
function imagesearchfield_plugin_list() {
  $plugins = imagesearchfield_get_imagesearch_engines();
  
  $plugin_info = array();
  foreach ($plugins as $plugin) {
    $plugin_info[] = theme('imagesearchfield_plugin_info', array('plugin' => $plugin));
  }
 
  $output = theme('item_list', array('items' => $plugin_info, 'type' => 'ul'));
  return $output;
}

/**
 * Settings page for a plugin in the system 
 */
function imagesearchfield_plugin_settings($plugin_name) {
  ctools_include('plugins');
  $function = ctools_plugin_load_function('imagesearchfield', 'imagesearch_engines', $plugin_name, 'settings form');
  
  if (NULL == $function) {
    return t("The plugin '" . $plugin_name . "' doesn't exist, or else has no settings form");
  }
  else {    
    $form = drupal_get_form($function);
    return $form;
  }
}
